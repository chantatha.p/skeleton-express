const router = require('express').Router()
const authentication = require('./authentication')
const users = require('./users')

router.get('/', authentication.middleware, users.getUser)

// router.get('/users', users.main)

module.exports = router
    