FROM node:8.12-alpine

WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY package.json /usr/src/app/
RUN npm i

COPY . /usr/src/app

# ENV PORT 5000
# EXPOSE $PORT
CMD [ "npm", "start" ]
